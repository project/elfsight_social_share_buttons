<?php

namespace Drupal\elfsight_social_share_buttons\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * {@inheritdoc}
 */
class ElfsightSocialShareButtonsController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function content() {
    $url = 'https://apps.elfsight.com/embed/social-share-buttons/?utm_source=portals&utm_medium=drupal&utm_campaign=social-share-buttons&utm_content=sign-up';

    require_once __DIR__ . '/embed.php';

    return [
      'response' => 1,
    ];
  }

}
